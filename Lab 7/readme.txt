      ___           ___           ___          _____                   ___           ___     
     /  /\         /  /\         /  /\        /  /::\                 /__/\         /  /\    
    /  /::\       /  /:/_       /  /::\      /  /:/\:\               |  |::\       /  /:/_   
   /  /:/\:\     /  /:/ /\     /  /:/\:\    /  /:/  \:\              |  |:|:\     /  /:/ /\  
  /  /:/~/:/    /  /:/ /:/_   /  /:/~/::\  /__/:/ \__\:|           __|__|:|\:\   /  /:/ /:/_ 
 /__/:/ /:/___ /__/:/ /:/ /\ /__/:/ /:/\:\ \  \:\ /  /:/          /__/::::| \:\ /__/:/ /:/ /\
 \  \:\/:::::/ \  \:\/:/ /:/ \  \:\/:/__\/  \  \:\  /:/           \  \:\~~\__\/ \  \:\/:/ /:/
  \  \::/~~~~   \  \::/ /:/   \  \::/        \  \:\/:/             \  \:\        \  \::/ /:/ 
   \  \:\        \  \:\/:/     \  \:\         \  \::/               \  \:\        \  \:\/:/  
    \  \:\        \  \::/       \  \:\         \__\/                 \  \:\        \  \::/   
     \__\/         \__\/         \__\/                                \__\/         \__\/    
     
CPE 185L -- LAB 7    
AUTHOR: Vaukee Lee
Section 2 Weds

This lab is a simple function test of the Universal TV remote configured as a Sony Tv remote in junction with an IR receiver.


For more information please see the Wiki https://bitbucket.org/vaukee/cpe-185l-weds/wiki/CPE%20185%20Lab%207

