      ___           ___           ___          _____                   ___           ___     
     /  /\         /  /\         /  /\        /  /::\                 /__/\         /  /\    
    /  /::\       /  /:/_       /  /::\      /  /:/\:\               |  |::\       /  /:/_   
   /  /:/\:\     /  /:/ /\     /  /:/\:\    /  /:/  \:\              |  |:|:\     /  /:/ /\  
  /  /:/~/:/    /  /:/ /:/_   /  /:/~/::\  /__/:/ \__\:|           __|__|:|\:\   /  /:/ /:/_ 
 /__/:/ /:/___ /__/:/ /:/ /\ /__/:/ /:/\:\ \  \:\ /  /:/          /__/::::| \:\ /__/:/ /:/ /\
 \  \:\/:::::/ \  \:\/:/ /:/ \  \:\/:/__\/  \  \:\  /:/           \  \:\~~\__\/ \  \:\/:/ /:/
  \  \::/~~~~   \  \::/ /:/   \  \::/        \  \:\/:/             \  \:\        \  \::/ /:/ 
   \  \:\        \  \:\/:/     \  \:\         \  \::/               \  \:\        \  \:\/:/  
    \  \:\        \  \::/       \  \:\         \__\/                 \  \:\        \  \::/   
     \__\/         \__\/         \__\/                                \__\/         \__\/    
     
CPE 185L -- LAB 8    
AUTHOR: Vaukee Lee
Section 2 Weds

This lab consists of 2 IR distance sensors of different measuring capabilities (one with further range for no good 
reason because i ordered them without completely thinking about it). They will be acting like trip sensors and for 
that time they tripped the time will be recorded and subtracted to find out the total time from one point to the other.
Once we get the travel time, we will divide by a set distance to get our speed.


For more information please see the Wiki https://bitbucket.org/vaukee/cpe-185l-weds/wiki/CPE%20185%20Lab%208