/*
 Sharp GP2Y0A21YK0F infrared proximity sensor (#28995)
 Displays raw digital value of approximate distance every 
 quarter second (250 milliseconds).
 
 Distance measurement range is 10cm to 80cm (3.9" to 31.5")
  
 This example code is for the Parallax Propeller Activity Board. Separate versions
 of these examples are provided for the Parallax Board of Education.
 
 Returns basic voltage measurement, and displays it in the SimpleIDE Terminal window. 
 Use Program->Run With Terminal to upload the program to the Activity Board. This
 also opens the SimpleIDE Terminal window. Be sure to set communication speed to 
 115200 baud.
 
 Typical values are from 0.2 (maximum distance) to 3.1, indicating 
 minimum distance. Values outside these ranges may occur when the object 
 is too close to the sensor (under 10cm)
 
 Keep in mind output is not linear to distance. See the Sharp GP2Y0A21YK0F 
 datasheet for more information.

Note: Be sue to download the latest Simple Libraries files from:
  learn.parallax.com/propeller-c-set-simpleide/update-your-learn-folder 
 
Connections:
Sensor       Propeller Propeller Activity Board
GND          GND
Vcc          5V
VO(utput)    Analog pin 0 (A0)

Note: The following standard color-coding is used with the adapter cable included
from Parallax with this sensor:

Color       Connection on sensor
Black       GND
Red         Vcc
White       VO
*/

#include "simpletools.h"                      // Include simpletools library
#include "adcDCpropab.h"                      // Include adcDCpropab library


int milliseconds;
int msTicks;
int tSync;

void background();

int main()
{
  float ad0, ad1;
  float cm = 20;
  float time1, time2;
  adc_init(21, 20, 19, 18);                   // Initialize ADC on Activity Board
  
  milliseconds = 0;
  cog_run(background, 128);

  while(1)                                    // Loop repeats indefinitely
  {
    ad0 = adc_volts(0);                      
    ad1 = adc_volts(1);
    
    putChar(HOME);                            // Set cursor to top left
    print("Reading: %f%c\n", ad0, CLREOL);    // Display voltage from sensor
    print("Reading: %f%c\n", ad1, CLREOL);
   
    if(ad0 > 2|| ad1 < 3){
        time1 = milliseconds;
    }
    if(ad1 > 2 || ad1 < 3){
        time2 = milliseconds;
    }      
     print("Reading: %f%c\n", cm/(time2-time1), CLREOL);
    
    pause(200);
    
    }
     
                           
 
}  
  void background(){
  msTicks = ((CLKFREQ) / 1000);
  tSync = (CNT);
  while (1) {
    tSync = (tSync + msTicks);
    waitcnt(tSync);
    milliseconds = (milliseconds + 1);
  }
  }